TARGETS := MAIN

#target
TARGET_MAIN             := ANNimator
TARGET_MAIN_SOURCE_DIRS := src

#compiler flags
TARGET_MAIN_CFLAGS  = -O3 -flto=${CORES} -std=c++1z -Wall -Weffc++
TARGET_MAIN_LDFLAGS = -fuse-linker-plugin
TARGET_MAIN_LDLIBS  = -lGL -lGLU -lglfw

#debug flags (in case .debug exists) (disable temporarily via make release)
TARGET_MAIN_DEBUG_CFLAGS  = -g -D__DEBUG__
TARGET_MAIN_DEBUG_LDFLAGS = -Wl,-z,now -rdynamic
TARGET_MAIN_DEBUG_LDLIBS  = -ldl -lbfd

include $(shell curator --makefile c++)

