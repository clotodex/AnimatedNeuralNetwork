
#include "common.h"

const Vector2<float> start(-1.f, 0.f);
const Vector2<float> v0(1.f, -0.5f);
const Vector2<float> v1(1.f, 0.5f);

const Vector3<float> ice(184.f / 255.f, 250.f / 255.f, 255.f / 255.f);
const Vector3<float> red(250.f / 255.f, 105.f / 255.f, 0.f);
