#ifndef TOOLS_H
#define TOOLS_H

long GetCurSysMillis();
void DrawNode(const Vector3<float>& color, GLfloat radius);
void DrawNodeFilled(const Vector3<float>& color, GLfloat radius);
void DrawVector(const Vector3<float>& color, Vector2<float> vector);
void DrawVector(const Vector3<float>& color, Vector2<float> from, Vector2<float> to);

#endif //TOOLS_H
