#include "common.h"

long GetCurSysMillis() {

	auto time = std::chrono::system_clock::now(); // get the current time

	auto since_epoch = time.time_since_epoch(); // get the duration since epoch

	// I don't know what system_clock returns
	// I think it's uint64_t nanoseconds since epoch
	// Either way this duration_cast will do the right thing
	auto millis = std::chrono::duration_cast<std::chrono::milliseconds>(since_epoch);

	long now = millis.count(); // just like java (new Date()).getTime();

	return now;

	//struct timeval time;

	//gettimeofday(&time, CLOCK_REALTIME); //durch CLOCK_REALTIME sind die einheinten auf verschiedenen sysgem (cputime vs usertime vs walltime) mit sicherheit geich
	////time.tv_sec  sind sekunden
	////time.tv_usec   sind mikrosekunden also sec/1000000

	////--> zu millis:
	//long millis = (time.tv_sec * 1000 + time.tv_usec / 1000);
	//return millis;
}

void DrawNode(const Vector3<float>& c, GLfloat a) {
	GLfloat half = a / 2;
	//glLineWidth(5.0f);

	GLfloat x = 0;
	GLfloat y = 0;
	GLfloat radius = half;

	glBegin(GL_LINES);
	glColor3f(c.r, c.g, c.b);
	for (int i = 0; i < 180; i++) {
		x = radius * cos(i);
		y = radius * sin(i);
		glVertex3f(x, y, 0);
		x = radius * cos(i + 0.1);
		y = radius * sin(i + 0.1);
		glVertex3f(x, y, 0);
	}
	glEnd();
}

void DrawNodeFilled(const Vector3<float>& c, GLfloat a) {
	GLfloat half = a / 2;
	//glLineWidth(5.0f);

	GLfloat x = 0;
	GLfloat y = 0;
	GLfloat radius = half;

	glBegin(GL_TRIANGLE_FAN);
	glColor3f(c.r, c.g, c.b);
	for (int i = 0; i < 180; i++) {
		x = radius * cos(i);
		y = radius * sin(i);
		glVertex3f(x, y, 0);
		x = radius * cos(i + 0.1);
		y = radius * sin(i + 0.1);
		glVertex3f(x, y, 0);
	}
	glEnd();
}

void DrawVector(const Vector3<float>& c, Vector2<float> v) {
	glBegin(GL_LINES);
	glColor3f(c.r, c.g, c.b);
	glVertex3f(0.f, 0.f, 0.f);
	glVertex3f(v.x, v.y, 0.f);
	glEnd();
}

void DrawVector(const Vector3<float>& c, Vector2<float> from, Vector2<float> to) {
	glBegin(GL_LINES);
	glColor3f(c.r, c.g, c.b);
	glVertex3f(from.x, from.y, 0.f);
	glVertex3f(to.x, to.y, 0.f);
	glEnd();
}

void render() {

	glMatrixMode(GL_MODELVIEW);      // To operate on Model-View matrix
	glLoadIdentity();                // Reset the model-view matrix

	//NODE 1
	glTranslatef(start.x, start.y, 0.f);// Translate left and up
	DrawNode(ice, 0.3f);
	glTranslatef(-start.x, -start.y, 0.0f); // Translate left and up

	glTranslatef(start.x, start.y, 0.f);// Translate left and up
	DrawVector(ice, v0);
	DrawVector(ice, v1);
	glTranslatef(-start.x, -start.y, 0.0f); // Translate left and up


	//NODE 2
	glTranslatef(start.x + v0.x, start.y + v0.y, 0.f); // Translate left and up
	DrawNode(ice, 0.3f);
	glTranslatef(-start.x - v0.x, -start.y - v0.y, 0.0f); // Translate left and up

	glTranslatef(start.x + v0.x, start.y + v0.y, 0.f); // Translate left and up
	DrawVector(ice, v1);
	glTranslatef(-start.x - v0.x, -start.y - v0.y, 0.0f); // Translate left and up

	//NODE 3
	glTranslatef(start.x + v1.x, start.y + v1.y, 0.f); // Translate left and up
	DrawNode(ice, 0.3f);
	glTranslatef(-start.x - v1.x, -start.y - v1.y, 0.0f); // Translate left and up

	glTranslatef(start.x + v1.x, start.y + v1.y, 0.f); // Translate left and up
	DrawVector(ice, v0);
	glTranslatef(-start.x - v1.x, -start.y - v1.y, 0.0f); // Translate left and up

	//NODE 4
	glTranslatef(start.x + v0.x + v1.x, start.y + v0.y + v1.y, 0.f); // Translate left and up
	DrawNode(ice, 0.3f);
	glTranslatef(-start.x - v0.x - v1.x, -start.y - v0.y - v1.y, 0.0f); // Translate left and up

}
