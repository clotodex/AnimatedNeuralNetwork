#ifndef __FEEDFORWARDANN__CONNECTION_H
#define __FEEDFORWARDANN__CONNECTION_H

class Connection {
private:
	Neuron* from;
	Neuron* to;
	double weight;
	long startTime = 0;
	long startTimeError = 0;

	double information;
	double error;

public:
	Connection(Neuron* from, Neuron* to, double weight);
	~Connection();
	void FeedForward(double information);
	void FeedForwardInstant(double information);
	void FeedBackwardError(double error);
	void FeedBackwardInstantError(double error);
	void Draw();
};

#endif //__FEEDFORWARDANN__CONNECTION_H
