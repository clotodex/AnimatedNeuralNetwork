#include "../common.h"

Connection::Connection(Neuron* from, Neuron* to, double weight) : from(from), to(to), weight(weight) {
}

Connection::~Connection(){
	//DELETE(from);
	//DELETE(to);
}

void Connection::FeedBackwardError(double error) {
	this->error = error;
	startTimeError = GetCurSysMillis();
}

void Connection::FeedBackwardInstantError(double error){
	//makes no save to even save it whene it is transferred directly
//	from->ProcessError(error);
}

void Connection::FeedForward(double information) {
	this->information = information;
	startTime = GetCurSysMillis();
}

void Connection::FeedForwardInstant(double information){
	//makes no save to even save it whene it is transferred directly
	//to->Process(information);
}

#define NEEDED_TIME 500

void Connection::Draw() {
	//auch die ice variable (color) hätte ich gern zugänglich aber woanders gespeichert
	Vector2<float> tmp;
	DrawVector(ice, *(from->pos), *(to->pos));
	if (startTime != 0) {
		double delta = (double)(GetCurSysMillis() - startTime) / NEEDED_TIME;
		if (delta > 1.0){
			delta = 1.0;
			startTime = 0;
			FeedForwardInstant(information);
			return;
		}

		delta = (sin(delta * M_PI - M_PI * 0.5) + 1.) / 2.;

		Vector2<float> cur;
		cur.x = from->pos->x + (to->pos->x - from->pos->x) * delta;
		cur.y = from->pos->y + (to->pos->y - from->pos->y) * delta;

		glTranslatef(cur.x, cur.y, 0.0f);
		glLineWidth(weight);
		DrawNodeFilled(ice, delta == 1.0 ? 0.2f : 0.1f); //make bubble bigger when arrived
		glTranslatef(-cur.x, -cur.y, 0.0f);
	}

	if (startTimeError != 0) {
		double delta = (double)(GetCurSysMillis() - startTimeError) / NEEDED_TIME;
		if (delta > 1.0){
			delta = 1.0;
			startTimeError = 0;
			FeedBackwardInstantError(error);
			return;
		}

		delta = (sin(delta * M_PI - M_PI * 0.5) + 1.) / 2.;

		Vector2<float> cur;
		cur.x = to->pos->x + (from->pos->x - to->pos->x) * delta;
		cur.y = to->pos->y + (from->pos->y - to->pos->y) * delta;

		glTranslatef(cur.x, cur.y, 0.0f);
		glLineWidth(weight);
		DrawNodeFilled(red, delta == 1.0 ? 0.2f : 0.1f); //make bubble bigger when arrived
		glTranslatef(-cur.x, -cur.y, 0.0f);
	}
}
