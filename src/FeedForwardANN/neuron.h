#ifndef __FEEDFORWARDANN__NEURON_H
#define __FEEDFORWARDANN__NEURON_H

class Neuron{
	public:
		double* weights = nullptr;
		double* deltavalues = nullptr;
		double output = 0;
		double gain = 0;
		double wgain = 0;

        Vector2<float>* pos = nullptr;
		Neuron(Vector2<float>* position, int inputcount);
		~Neuron();
		void Draw();
};

#endif //__FEEDFORWARDANN__NEURON_H
