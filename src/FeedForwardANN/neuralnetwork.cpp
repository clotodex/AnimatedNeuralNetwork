#include "../common.h"

NeuralNetwork::NeuralNetwork(Vector2<float>* pos, int inputcount, int inputneurons, int outputcount, int* hiddenlayers, int hiddenlayercount) {
	//make sure required values are not zero
	if (inputcount <= 0 || inputneurons <= 0 || outputcount <= 0) {
		LOG("init network with negative or 0 values!", LOGLEVEL_FATAL);
		return;
	}
	float stepx = 2.f / (hiddenlayercount + 2);
	float setx = -1.f;
	setx += stepx;

	int i;

	LOG("creating inputlayer..", LOGLEVEL_DEBUG);
	inputlayer = new Layer(new Vector2<float>(setx, -1.f), inputcount, inputneurons);
	setx += stepx;
	if (hiddenlayers && hiddenlayercount) {
		this->hiddenlayers = new Layer*[hiddenlayercount];
		this->hiddenlayercount = hiddenlayercount;
		for (i = 0; i < hiddenlayercount; i++) {
			if (i == 0) {
				//first hidden Layer receives the output of the inputlayer so we set as input the neuroncount
				//of the inputlayer

				LOG("creating first hidden layer..", LOGLEVEL_DEBUG);
				this->hiddenlayers[i] = new Layer(new Vector2<float>(setx, (setx - stepx)), inputneurons, hiddenlayers[i]);
				setx += stepx;
			} else {
				LOG("creating hidden layer..", LOGLEVEL_DEBUG);
				this->hiddenlayers[i] = new Layer(new Vector2<float>(setx, (setx - stepx)), hiddenlayers[i - 1], hiddenlayers[i]);
				setx += stepx;
			}
		}
		LOG("creating outputlayer for hidden..", LOGLEVEL_DEBUG);
		outputlayer = new Layer(new Vector2<float>(setx, (setx - stepx)), hiddenlayers[hiddenlayercount - 1], outputcount);
		setx += stepx;
	} else {
		LOG("creating outputlayer..", LOGLEVEL_DEBUG);
		outputlayer = new Layer(new Vector2<float>(setx, (setx - stepx)), inputneurons, outputcount);
		setx += stepx;
	}
	this->pos = pos;
}

NeuralNetwork::~NeuralNetwork() {
	for (int i = 0; i < hiddenlayercount; i++)
		DELETE(hiddenlayers[i]);
	DELETE_ARRAY(hiddenlayers);
	DELETE(inputlayer);
	DELETE(outputlayer);
	DELETE(pos);
}

Layer* NeuralNetwork::getOutput() {
	return outputlayer;
}


void NeuralNetwork::propagate(double* input) {
	if (animateIndex >= 0) {
		animateinput = input;
		return;
	}

	animateinput = nullptr;

	//The propagation function should start from the input Layer
	//first copy the input Vector to the input Layer Always make sure the size
	//"array input" has the same size of inputcount

	//memcpy(inputlayer->inputs, input, inputlayer->inputcount * sizeof(double));
	//TODO somehow crosscheck input count and inputscount
	for (int i = 0; i < inputlayer->inputcount; i++)
		inputlayer->inputs[i] = input[i];

	//now calculate the inputlayer
	inputlayer->calculate();

	update(-1);//propagate the inputlayer out values to the next Layer
	if (hiddenlayers) {
		//Calculating hidden layers if any
		for (int i = 0; i < hiddenlayercount; i++) {
			hiddenlayers[i]->calculate();
			update(i);
		}
	}

	//calculating the final statge: the output Layer
	outputlayer->calculate();
}

void NeuralNetwork::setanimate(bool animate) {
	this->animateIndex = animate ? 0 : -1;
	inputlayer->setanimate(animate);
	for (int i = 0; i < hiddenlayercount; i++){
		printf("activating hidden layers\n");
		hiddenlayers[i]->setanimate(animate);
	}
	outputlayer->setanimate(animate);
}

void NeuralNetwork::animatepropagate() {

	static int lastIndex = -1;
	//reset
	if (animateIndex > hiddenlayercount) {
		printf("OUT: %f\n", outputlayer->neurons[0]->output);
		setanimate(false);
		lastIndex = -1;
	}
	//increment when done
	Layer* tmp = nullptr;
	if (lastIndex != -1) {
		if (animateIndex == 0)
			tmp = inputlayer;
		else if (animateIndex - 1 < hiddenlayercount) {
			if (animateIndex > hiddenlayercount)
				printf("animatepropagate - %s: %d, %s: %d\n", "accessing", animateIndex - 1, "max", hiddenlayercount);
			tmp = hiddenlayers[animateIndex - 1];
		} else
			tmp = outputlayer;
	}
	if (tmp) {
//			printf("%s %d\n","time: ",tmp->startTime);
		unsigned long zero = 0l;
		if (tmp->startTime == zero){
			animateIndex++;
			printf("%s %d\n","increment to:", animateIndex);
		}
	}

	if (lastIndex == animateIndex){
		return;
	}
	lastIndex = animateIndex;
	printf("got here\n");
	if (animateIndex == 0) { //inputlayer
		for (int i = 0; i < inputlayer->inputcount; i++)
			inputlayer->inputs[i] = animateinput[i];

		//now calculate the inputlayer
		inputlayer->calculate();
		printf("calculating input layer\n");

	} else {
		update(animateIndex - 2); //propagate the inputlayer out values to the next Layer
		if (animateIndex-1 < hiddenlayercount){ //hiddenlayers exist
			hiddenlayers[animateIndex - 1]->calculate();
			printf("calculating middle layers\n");
		}else{
			outputlayer->calculate();
			printf("calculating output layer\n");
		}
	}
}

//Main training function. Run this function in a loop as many times needed per pattern
double NeuralNetwork::train(const double* desiredoutput, double* input, double alpha, double momentum) {
	//function train, teaches the network to recognize a pattern given a desired output
	double errorg = 0; //general quadratic error
	double errorc; //local error;
	double sum = 0, csum = 0;
	double delta, udelta;
	double output;
	//first we begin by propagating the input
	propagate(input);
	int i, j, k;
	//the backpropagation algorithm starts from the output Layer propagating the error  from the output
	//Layer to the input Layer
	LOG("calculating output errors", LOGLEVEL_DEBUG);
	for (i = 0; i < outputlayer->neuroncount; i++) {
		//calculate the error value for the output Layer
		output = outputlayer->neurons[i]->output; //copy this value to facilitate calculations
		//from the algorithm we can take the error value as
		errorc = (desiredoutput[i] - output) * output * (1 - output);
		//and the general error as the sum of delta values. Where delta is the squared difference
		//of the desired value with the output value
		//quadratic error
		errorg += (desiredoutput[i] - output) * (desiredoutput[i] - output) ;

		//now we proceed to update the weights of the neuron
		for (j = 0; j < outputlayer->inputcount; j++) {
			//get the current delta value
			delta = outputlayer->neurons[i]->deltavalues[j];
			//update the delta value
			udelta = alpha * errorc * outputlayer->inputs[j] + delta * momentum;
			//update the weight values
			outputlayer->neurons[i]->weights[j] += udelta;
			outputlayer->neurons[i]->deltavalues[j] = udelta;

			//we need this to propagate to the next Layer
			sum += outputlayer->neurons[i]->weights[j] * errorc;
		}

		//calculate the weight gain
		outputlayer->neurons[i]->wgain += alpha * errorc * outputlayer->neurons[i]->gain;

	}

	for (i = (hiddenlayercount - 1); i >= 0; i--) {
		LOG("calculating hidden errors", LOGLEVEL_DEBUG);
		for (j = 0; j < hiddenlayers[i]->neuroncount; j++) {
			output = hiddenlayers[i]->neurons[j]->output;
			//calculate the error for this Layer
			errorc = output * (1 - output) * sum;
			//update neuron weights
			for (k = 0; k < hiddenlayers[i]->inputcount; k++) {
				delta = hiddenlayers[i]->neurons[j]->deltavalues[k];
				udelta = alpha * errorc * hiddenlayers[i]->inputs[k] + delta * momentum;
				hiddenlayers[i]->neurons[j]->weights[k] += udelta;
				hiddenlayers[i]->neurons[j]->deltavalues[k] = udelta;
				csum += hiddenlayers[i]->neurons[j]->weights[k] * errorc; //needed for next Layer

			}

			hiddenlayers[i]->neurons[j]->wgain += alpha * errorc * hiddenlayers[i]->neurons[j]->gain;

		}
		sum = csum;
		csum = 0;
	}

	//and finally process the input Layer
	LOG("calculating input errors", LOGLEVEL_DEBUG);
	for (i = 0; i < inputlayer->neuroncount; i++) {
		output = inputlayer->neurons[i]->output;
		errorc = output * (1 - output) * sum;

		for (j = 0; j < inputlayer->inputcount; j++) {
			delta = inputlayer->neurons[i]->deltavalues[j];
			udelta = alpha * errorc * inputlayer->inputs[j] + delta * momentum;
			//update weights
			inputlayer->neurons[i]->weights[j] += udelta;
			inputlayer->neurons[i]->deltavalues[j] = udelta;
		}
		//and update the gain weight
		inputlayer->neurons[i]->wgain += alpha * errorc * inputlayer->neurons[i]->gain;
	}

	//return the general error divided by 2
	return errorg / 2;

}

void NeuralNetwork::update(int layerindex) {
	int i;
	if (layerindex == -1) {
		//dealing with the inputlayer here and propagating to the next Layer
		for (i = 0; i < inputlayer->neuroncount; i++) {
			if (hiddenlayers) //propagate to the first hidden Layer
				hiddenlayers[0]->inputs[i] = inputlayer->neurons[i]->output;
			else //propagate directly to the output Layer
				outputlayer->inputs[i] = inputlayer->neurons[i]->output;
		}

	} else {
		if (layerindex >= hiddenlayercount)
			printf("update - %s: %d, %s: %d\n", "accessing", layerindex, "max", hiddenlayercount);
		for (i = 0; i < hiddenlayers[layerindex]->neuroncount; i++) {
			//not the last hidden Layer
			if (layerindex < hiddenlayercount - 1) {
				//what if layerindex+1 has fewer neurons than layerindex
				if (hiddenlayers[layerindex + 1]->inputcount <= i)
					LOG("YOU DID SOMETHING VERY VERY BAD!!!", LOGLEVEL_FATAL);
				hiddenlayers[layerindex + 1]->inputs[i] = hiddenlayers[layerindex]->neurons[i]->output;
			} else
				outputlayer->inputs[i] = hiddenlayers[layerindex]->neurons[i]->output;
		}
	}
}
void NeuralNetwork::Draw() {
	//TODO set black background
	//for (int i = 0; i < neuroncount; i++)
	//  neurons[i]->Draw();

	float stepy = 2.f / (inputlayer->inputcount + 1);
	float sety = -1.f;
	for (int i = 0; i < inputlayer->inputcount; i++) {
		sety += stepy;
		glTranslatef(-1.f, sety, 0.f);
		DrawNode(ice, 0.3f);
		glTranslatef(1.f, -sety, 0.f);
	}

	inputlayer->Draw();
	for (int i = 0; i < hiddenlayercount; i++)
		hiddenlayers[i]->Draw();
	outputlayer->Draw();

	if (animateIndex >= 0 && animateinput)
		animatepropagate();
}



