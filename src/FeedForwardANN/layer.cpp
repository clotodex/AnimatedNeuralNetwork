#include "../common.h"

Layer::Layer(Vector2<float>* pos, int inputsize, int _neuroncount) : pos(pos) {
	if (inputsize <= 0 || _neuroncount <= 0) {
		LOG("Negative values constructing Layer!", LOGLEVEL_FATAL);
		printf("%s: %d, %s: %d\n", "inputsize", inputsize, "_neuroncount", _neuroncount);
		return;
	}
	LOG("Layer created!", LOGLEVEL_DEBUG);

	float stepy = 2.f / (_neuroncount + 1);
	float sety = -1.f;
	sety += stepy;

	int i;
	neurons = new Neuron*[_neuroncount];
	for (i = 0; i < _neuroncount; i++) {
		//TODO calculate real position
		neurons[i] = new Neuron(new Vector2<float>(pos->x, sety), inputsize);
		sety += stepy;
	}

	inputs = new double[inputsize];
	neuroncount = _neuroncount;
	inputcount = inputsize;
}

Layer::~Layer() {
	for (int i = 0; i < neuroncount; i++)
		DELETE(neurons[i]);
	DELETE_ARRAY(neurons);
	DELETE_ARRAY(inputs);
	DELETE(pos);
}

void Layer::calculateInstant() {
	int i, j;
	double sum;
	//Apply the formula for each Neuron
	for (i = 0; i < neuroncount; i++) {
		sum = 0; //store the sum of all values here
		for (j = 0; j < inputcount; j++) {
			//Performing function
			sum += neurons[i]->weights[j] * inputs[j]; //apply input * weight
		}
		sum += neurons[i]->wgain * neurons[i]->gain; //apply the gain or theta multiplied by the gain weight.
		//sigmoidal activation function
		neurons[i]->output = 1.0 / (1.0 + exp(-sum)); //calculate the sigmoid function
		// neurons[i]->output=-1 + 2*(1.f + exp(-sum));
	}
}

void Layer::calculate() {
	if (!animate){
		calculateInstant();
		return;
	}
	startTime = GetCurSysMillis();
	//busy waiting until animation time is over
	//while (startTime != 0) {
	//}
	//calculateInstant();
}

void Layer::setanimate(bool animate) {
	this->animate = animate;
}

#define NEEDED_TIME 1500
void Layer::Draw() {

	float stepy = 2.f / (inputcount + 1);
	float sety = -1.f;
	sety += stepy;

	for (int i = 0; i < neuroncount; i++) {
		float sety = -1.f;
		for (int j = 0; j < inputcount; j++) {
			sety += stepy;
			glLineWidth(1.0+neurons[i]->weights[j]);
			DrawVector(ice, Vector2<float>(pos->y, sety) , *(neurons[i]->pos));
			glLineWidth(1.0);
		}
	}

	for (int i = 0; i < neuroncount; i++)
		neurons[i]->Draw();

	for (int i = 0; i < neuroncount; i++) {
		float sety = -1.f;
		for (int j = 0; j < inputcount; j++) {
			sety += stepy;

            Vector2<float> from(pos->y, sety);
            Vector2<float>* to = neurons[i]->pos;
			Vector2<float> tmp;
			if (startTime != 0) {
				double delta = (double)(GetCurSysMillis() - startTime) / NEEDED_TIME;
				if (delta > 1.0) {
					delta = 1.0;
					startTime = 0;
					calculateInstant();
					return;
				}

				delta = (sin(delta * M_PI - M_PI * 0.5) + 1.) / 2.;

				Vector2<float> cur;
				cur.x = from.x + (to->x - from.x) * delta;
				cur.y = from.y + (to->y - from.y) * delta;

				glTranslatef(cur.x, cur.y, 0.0f);
				//DrawNodeFilled(ice, delta == 1.0 ? 0.2f : 0.1f); //make bubble bigger when arrived
				float big = inputs[j]/3.f+0.05f;
				DrawNodeFilled(ice, big); //make bubble bigger when arrived
				glTranslatef(-cur.x, -cur.y, 0.0f);
			}
		}
	}
}
