#ifndef __FEEDFORWARDANN__COMMON_H
#define __FEEDFORWARDANN__COMMON_H

#include "neuron.h"
#include "connection.h"
#include "layer.h"
#include "neuralnetwork.h"
#include "annmain.h"

#endif //__FEEDFORWARDANN__COMMON_H
