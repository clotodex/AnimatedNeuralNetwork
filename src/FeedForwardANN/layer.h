#ifndef __FEEDFORWARDANN__LAYER_H
#define __FEEDFORWARDANN__LAYER_H

class Layer {
	private:
		bool animate = false;
	public:
		unsigned long startTime = 0;
		Neuron** neurons = nullptr;
		int neuroncount = 0;
		double* inputs = nullptr;
		int inputcount = 0;
		Vector2<float>* pos = nullptr;

        Layer(Vector2<float>* pos, int inputsize, int neuroncount);
		~Layer();
		void calculateInstant();
		void calculate();
		void setanimate(bool animate);
		void Draw();
};

#endif // __FEEDFORWARDANN__LAYER_H
