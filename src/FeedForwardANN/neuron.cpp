#include "../common.h"

Neuron::Neuron(Vector2<float>* position, int inputcount) : pos(position){
	LOG("Neuron created!", LOGLEVEL_DEBUG);
    if(inputcount <= 0){
		LOG("inputcount cannot be lower or equal to 0!", LOGLEVEL_FATAL);
	}
	double sign=-1;//to change sign
    double random;//to get random number
    weights=new double[inputcount];
    deltavalues=new double[inputcount];
    //important initializate all weights as random unsigned values
    //and delta values as 0
    for(int i=0;i<inputcount;i++)
    {
        //get a random number between -0.5 and 0.5
        random=RND_INCL(-0.5, 0.5); //min 0.5
        random*=sign;
        sign*=-1;
        weights[i]=random;
        deltavalues[i]=0;
    }
    gain=1;

    random=RND_INCL(-0.5, 0.5); //min 0.5
    random*=sign;
    sign*=-1;
    wgain=random;
}

Neuron::~Neuron(){
	DELETE_ARRAY(weights);
	DELETE_ARRAY(deltavalues);
	DELETE(pos);
}

//TODO check if yet needed
void Neuron::Draw(){
	glTranslatef(pos->x, pos->y, 0.f);
	DrawNodeFilled(Vector3<float>(0.f,0.f,0.f), 0.3f);
	DrawNode(ice, 0.3f);
	glTranslatef(-pos->x, -pos->y, 0.f);
}
