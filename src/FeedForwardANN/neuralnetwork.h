#ifndef __FEEDFORWARDANN__NEURALNETWORK_H
#define __FEEDFORWARDANN__NEURALNETWORK_H

class NeuralNetwork {
	private:
		int hiddenlayercount = 0;
        Layer* inputlayer = nullptr;
        Layer* outputlayer = nullptr;
		Layer** hiddenlayers = nullptr;
		Vector2<float>* pos = nullptr;
	public:
		int animateIndex = -1;
		double* animateinput = nullptr;
		NeuralNetwork(Vector2<float>* pos, int inputcount,int inputneurons,int outputcount,int *hiddenlayers,int hiddenlayercount);
		~NeuralNetwork();

		void propagate(double *input);
		void animatepropagate();
		double train(const double *desiredoutput,double *input,double alpha, double momentum);
		void update(int layerindex);
        Layer* getOutput();
		void setanimate(bool animate);

		//OLD
		void Draw();
};

#endif //__FEEDFORWARDANN__NEURALNETWORK_H
