#include "../common.h"

void error_callback(int error, const char* description) {
	dprintf(STDOUT_FILENO, "%s\n", description);
}

#define PATTERN_COUNT 4
#define PATTERN_SIZE 2
#define NETWORK_INPUTNEURONS 3
#define NETWORK_OUTPUT 1
#define HIDDEN_LAYERS 0
#define EPOCHS 2000000
NeuralNetwork* nn = nullptr;
double pattern[PATTERN_COUNT][PATTERN_SIZE] = {
	{0, 0},
	{0, 1},
	{1, 0},
	{1, 1}
};

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods) {
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);
	if (key == GLFW_KEY_ENTER && action == GLFW_PRESS) {
		if (nn) {
			//TODO somehow do all patterns --> threads!!!
				nn->setanimate(true);
				nn->propagate(pattern[(int)RND(0,5)]);
				printf("%s: %d, %s: %f\n", "TESTED PATTERN", 0,"NET RESULT", nn->getOutput()->neurons[0]->output);
		}
	}
}

void ResizeHandler(GLFWwindow* window, GLsizei width, GLsizei height) {
	// Compute aspect ratio of the new window
	if (height == 0) height = 1;                // To prevent divide by 0
	GLfloat aspect = (GLfloat)width / (GLfloat)height;

	// Set the viewport to cover the new window
	glViewport(0, 0, width, height);

	// Set the aspect ratio of the clipping area to match the viewport
	glMatrixMode(GL_PROJECTION);  // To operate on the Projection matrix
	glLoadIdentity();             // Reset the projection matrix
	if (width >= height) {
		// aspect >= 1, set the height from -1 to 1, with larger width
		glOrtho(-1.0 * aspect, 1.0 * aspect, -1.0, 1.0, 1.f, -1.f);
	} else {
		// aspect < 1, set the width to -1 to 1, with larger height
		glOrtho(-1.0, 1.0, -1.0 / aspect, 1.0 / aspect, 1.f, -1.f);
	}
}


GLFWwindow* window;
void initNetwork() {
	//Create some patterns
	//playing with xor
	//XOR input values

	//XOR desired output values
	double desiredout[PATTERN_COUNT][NETWORK_OUTPUT] = {
		{0},
		{1},
		{1},
		{0}
	};

	int hiddenlayercount = 1;
	int hl[hiddenlayercount] = {
		2
	};
//	hiddenlayercount = 0;
	int* hiddenlayers = hl;//hl;

	int i, j;
	double error;
	//We create the network
	for (int a = 0; a < 10; a++) {
		nn = new NeuralNetwork(new Vector2<float>(0, 0), PATTERN_SIZE, NETWORK_INPUTNEURONS, NETWORK_OUTPUT, hiddenlayers, hiddenlayercount);
		printf("---------------------------------------------------------------------------\n");

		double momentum = 0.01;
		double alpha = 0.1;
		//Start the neural network training
		double lasterror = 20;
		bool earlydetect = false;
		for (i = 0; i < EPOCHS; i++) {
			error = 0;
			for (j = 0; j < PATTERN_COUNT; j++)
				error += nn->train(desiredout[j], pattern[j], momentum, alpha);
			error /= PATTERN_COUNT;
			//display error
			//printf("%s: %f\n", "error", error);
			printf("\r%f prozent", ((i + 1) / (double)EPOCHS));
			if (fabs(lasterror - error) < 0.00000000001 && error > 0.0001) {
				printf("%f - %f\n", error, lasterror);
				LOG("DETECTED BAD EARLY", LOGLEVEL_FATAL);
				earlydetect = true;
				break;
			}
			lasterror = error;
		}
		printf("\n");
		if (earlydetect){
			DELETE(nn);
			continue;
		}
		printf("%s: %f\n", "yet exisiting error", error);

		//once trained test all patterns
		bool allgood = true;
		for (i = 0; i < PATTERN_COUNT; i++) {

			nn->propagate(pattern[i]);

			printf("%s: %d, %s: %f, %s: %f\n", "TESTED PATTERN", i, "DESIRED OUTPUT", *desiredout[i], "NET RESULT", nn->getOutput()->neurons[0]->output);
			if (((*desiredout[i]) - (nn->getOutput()->neurons[0]->output)) > 0.1)
				allgood = false;
		}
		if (lasterror > 0.0001)
			LOG("POSSIBLE BAD RESULT", LOGLEVEL_WARNING);
		if (allgood) {
			LOG("perfect", LOGLEVEL_INFO);
			return;
		} else
			LOG("BAD", LOGLEVEL_ERROR);
		DELETE(nn);
	}
	glfwSetWindowShouldClose(window, GL_TRUE);
}

void deinitNetwork() {
	DELETE(nn);
	nn = nullptr;
}

int ticks = 0;

int mainANN() {
	glfwSetErrorCallback(error_callback);
	if (!glfwInit())
		exit(EXIT_FAILURE);
	window = glfwCreateWindow(720, 480, "Simple example", NULL, NULL);
	if (!window) {
		glfwTerminate();
		exit(EXIT_FAILURE);
	}
	glfwMakeContextCurrent(window);
	glfwSwapInterval(1);
	glfwSetKeyCallback(window, key_callback);
	glfwSetWindowSizeCallback(window, ResizeHandler);
	unsigned long last = 0;
	unsigned long timestep = 1000;

	ResizeHandler(window, 720, 480);

	GLfloat LineRange[2];
	glGetFloatv(GL_LINE_WIDTH_RANGE, LineRange);
	printf("Minimum/Maximum Line Width ");
	for (int i = 0; i < 2; i++)
		printf("arr[%d] = %f\n", i, LineRange[i]);

//	setupVerticesAndColors();

	initNetwork();

	while (!glfwWindowShouldClose(window)) {
		int width, height;

		glfwGetFramebufferSize(window, &width, &height);

		//ratio = width / (float) height;
		//glViewport(0, 0, width, height);
		////glClear(GL_COLOR_BUFFER_BIT);
		//glMatrixMode(GL_PROJECTION);
		//glLoadIdentity();
		//glOrtho(-ratio, ratio, -1.f, 1.f, 1.f, -1.f);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		//glRotatef((float) glfwGetTime() * 50.f, 0.f, 0.f, 1.f);
		//glBegin(GL_TRIANGLES);
		//glColor3f(1.f, 0.f, 0.f);
		//glVertex3f(-0.6f, -0.4f, 0.f);
		//glColor3f(0.f, 1.f, 0.f);
		//glVertex3f(0.6f, -0.4f, 0.f);
		//glColor3f(0.f, 0.f, 1.f);
		//glVertex3f(0.f, 0.6f, 0.f);

		unsigned long cur = GetCurSysMillis();
		if (cur - last > timestep) {
			last = cur;
			//TODO make animation
			printf("tick\n");
			ticks++;
		}

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		nn->Draw();

		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	deinitNetwork();

	glfwDestroyWindow(window);
	glfwTerminate();
	exit(EXIT_SUCCESS);
}
