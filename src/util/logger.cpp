#include "../common.h"

void LOG(char* message, int loglevel){
	int output = STDOUT_FILENO;
	char* format = 0;

	switch (loglevel)
	{
		case LOGLEVEL_FATAL:
			output = STDERR_FILENO;
			format = LOGCOLOR_FATAL "FATAL:" LOGCOLOR_RESET;
			break;

		case LOGLEVEL_ERROR:
			output = STDERR_FILENO;
			format = LOGCOLOR_ERROR "ERROR:" LOGCOLOR_RESET;
			break;

		case LOGLEVEL_WARNING:
			format = LOGCOLOR_WARNING "WARNING:" LOGCOLOR_RESET;
			break;

		case LOGLEVEL_INFO:
			format = LOGCOLOR_INFO "INFO:" LOGCOLOR_RESET;
			break;

		case LOGLEVEL_DEBUG:
			return;
			format = LOGCOLOR_DEBUG "DEBUG:" LOGCOLOR_RESET;
			break;

		case LOGLEVEL_VERBOSE:
		default:
			format = LOGCOLOR_VERBOSE "VERBOSE:" LOGCOLOR_RESET;
			break;
	}
	dprintf(output, format, loglevel);
	dprintf(output, message);
	dprintf(output, "\n");
}
