#ifndef __UTIL__VECTOR2_H
#define __UTIL__VECTOR2_H

template<class T> class Vector2 {
public:
	union {
		T data[2];

		struct { T x, y; } ATTRIBUTE_PACKED;
		struct { T u, v; } ATTRIBUTE_PACKED;
	} ATTRIBUTE_PACKED;

	Vector2() : x(0), y(0) {
	}

	Vector2(const T x, const T y) : x(x), y(y) {
	}

	Vector2(const Vector2<T>& vector) : x(vector.x), y(vector.y) {
	}

	Vector2<T>& operator()(const T x, const T y) {
		this->x = x;
		this->y = y;
		return *this;
	}

	Vector2<T>& operator=(const Vector2<T>& vector) {
		this->x = vector.x;
		this->y = vector.y;
		return *this;
	}

	Vector2<T>& operator+=(const Vector2<T>& vector) {
		this->x += vector.x;
		this->y += vector.y;
		return *this;
	}

	Vector2<T>& operator-=(const Vector2<T>& vector) {
		this->x -= vector.x;
		this->y -= vector.y;
		return *this;
	}

	Vector2<T>& operator*=(const Vector2<T>& vector) {
		this->x *= vector.x;
		this->y *= vector.y;
		return *this;
	}

	Vector2<T>& operator*=(const T& scalar) {
		this->x *= scalar;
		this->y *= scalar;
		return *this;
	}

	Vector2<T>& operator/=(const Vector2<T>& vector) {
		this->x /= vector.x;
		this->y /= vector.y;
		return *this;
	}

	Vector2<T>& operator/=(const T& scalar) {
		this->x /= scalar;
		this->y /= scalar;
		return *this;
	}

	Vector2<T> operator+(const Vector2<T>& vector) const {
		return Vector2<T>(x + vector.x, y + vector.y);
	}

	Vector2<T> operator-(const Vector2<T>& vector) const {
		return Vector2<T>(x - vector.x, y - vector.y);
	}

	Vector2<T> operator*(const Vector2<T>& vector) const {
		return Vector2<T>(x * vector.x, y * vector.y);
	};

	Vector2<T> operator/(const Vector2<T>& vector) const {
		return Vector2<T>(x / vector.x, y / vector.y);
	};

	Vector2<T> operator*(const T scalar) const {
		return Vector2<T>(x * scalar, y * scalar);
	};

	Vector2<T> operator/(const T scalar) const {
		return Vector2<T>(x / scalar, y / scalar);
	};

	Vector2<T> operator-() const {
		return Vector2<T>(-x, -y);
	};
};

#endif //__UTIL__VECTOR2_H
