#ifndef __UTIL__VECTOR3_H
#define __UTIL__VECTOR3_H

template<class T> class Vector3 {
public:
	union {
		T data[3];

		struct { T x, y, z; } ATTRIBUTE_PACKED;
		struct { T r, g, b; } ATTRIBUTE_PACKED;
	} ATTRIBUTE_PACKED;

	Vector3() : x(0), y(0), z(0) {
	}

	Vector3(const T x, const T y, const T z) : x(x), y(y), z(z) {
	}

	Vector3(const Vector3<T>& vector) : x(vector.x), y(vector.y), z(vector.z) {
	}

	Vector3<T>& operator()(const T x, const T y, const T z) {
		this->x = x;
		this->y = y;
		this->z = z;
		return *this;
	}

	Vector3<T>& operator=(const Vector3<T>& vector) {
		this->x = vector.x;
		this->y = vector.y;
		return *this;
	}

	Vector3<T>& operator+=(const Vector3<T>& vector) {
		this->x += vector.x;
		this->y += vector.y;
		return *this;
	}

	Vector3<T>& operator-=(const Vector3<T>& vector) {
		this->x -= vector.x;
		this->y -= vector.y;
		return *this;
	}

	Vector3<T>& operator*=(const Vector3<T>& vector) {
		this->x *= vector.x;
		this->y *= vector.y;
		return *this;
	}

	Vector3<T>& operator*=(const T& scalar) {
		this->x *= scalar;
		this->y *= scalar;
		return *this;
	}

	Vector3<T>& operator/=(const Vector3<T>& vector) {
		this->x /= vector.x;
		this->y /= vector.y;
		return *this;
	}

	Vector3<T>& operator/=(const T& scalar) {
		this->x /= scalar;
		this->y /= scalar;
		return *this;
	}

	Vector3<T> operator+(const Vector3<T>& vector) const {
		return Vector3<T>(x + vector.x, y + vector.y);
	}

	Vector3<T> operator-(const Vector3<T>& vector) const {
		return Vector3<T>(x - vector.x, y - vector.y);
	}

	Vector3<T> operator*(const Vector3<T>& vector) const {
		return Vector3<T>(x * vector.x, y * vector.y);
	};

	Vector3<T> operator/(const Vector3<T>& vector) const {
		return Vector3<T>(x / vector.x, y / vector.y);
	};

	Vector3<T> operator*(const T scalar) const {
		return Vector3<T>(x * scalar, y * scalar);
	};

	Vector3<T> operator/(const T scalar) const {
		return Vector3<T>(x / scalar, y / scalar);
	};

	Vector3<T> operator-() const {
		return Vector3<T>(-x, -y);
	};
};

#endif //__UTIL__VECTOR3_H
