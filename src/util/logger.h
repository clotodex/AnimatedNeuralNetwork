#ifndef __UTIL__LOGGER_H
#define __UTIL__LOGGER_H

#define LOGLEVEL_FATAL      0
#define LOGLEVEL_ERROR      1
#define LOGLEVEL_WARNING    2
#define LOGLEVEL_INFO       3
#define LOGLEVEL_DEBUG      4
#define LOGLEVEL_VERBOSE    5

#define LOGCOLOR_FATAL      "[1;31;40m"
#define LOGCOLOR_ERROR      "[1;37;41m"
#define LOGCOLOR_WARNING    "[1;43m"
#define LOGCOLOR_INFO       "[1;42m"
#define LOGCOLOR_DEBUG      "[1;46m"
#define LOGCOLOR_VERBOSE    "[1;47m"

#define LOGCOLOR_RESET      "[m"


void LOG(char* message, int loglevel);

#endif //__UTIL__LOGGER_H
