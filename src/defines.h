#ifndef __DEFINES_H
#define __DEFINES_H



//project defines






//attributes

#define ATTRIBUTE_PACKED           __attribute((packed))
#define DEPRECATED                 __attribute__((deprecated))
#define DEPRECATED_MSG(msg)        __attribute__((deprecated(msg)))


//functions

#define SEGFAULT()                 __builtin_trap();

#define OPEN_NAMESPACE(a)          namespace a {
#define CLOSE_NAMESPACE(a)         }

#define DELETE(var)                do { if (var) { delete var; var = 0; } } while (false)
#define DELETE_ARRAY(var)          do { if (var) { delete [] var; var = 0; } } while (false)

#define RND(min, max)             (((double)rand() / RAND_MAX) * (max - min) + min)


#define RND_INCL(min, max)             (((double)rand() / (RAND_MAX - 1)) * (max - min) + min)

#endif //__DEFINES_H
