Change Log
==========

All notable changes to this project will be documented in this file. This project adheres to [Semantic Versioning](semver.org).

***

## Unreleased
### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security

***

## 0.0.1 - 2016-1-26

### Added
- Basic FeedForward Artificial Network with BackPropagation learning.
- Hard implemented animation of this network.
- Example implementation for the XOR Problem.

***