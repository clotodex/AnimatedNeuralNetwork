ANNimator - Artificial Neural Networks being animated
=====================================================

Features/Aims:
--------------

- Library for *common* artificial neural networks
- Library for creating *own* artificial neural networks
- Capability to *animate* all neural networks
- Example applications
    
Good to know:
-------------

- the **versioning** system is described here: [semantic versioning](http://semver.org/ "semver.org")
- the **changelog** follows this [guide](https://github.com/olivierlacan/keep-a-changelog "changelog guide")
- for now this work is **licensed** under a [Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License](http://creativecommons.org/licenses/by-nc-nd/4.0/)